#!/usr/bin/python3

import os
from pathlib import Path
import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from .api.utils import tags_metadata, AddRoutes
from .config import Load as LoadConfig

#Load configuration
LoadConfig()

#App setup
app = FastAPI(
	title="Reckoner Sync Server",
	description="A sync server to make sure Reckoner clients are securly and properly synchronized.",
	openapi_tags=tags_metadata,
	version="0.1"
)
AddRoutes(app)

if os.path.isdir("./web"):
	app.mount("/", StaticFiles(directory="./web", html = True), name="web")

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
	filename = Path(__file__).stem
	uvicorn.run(f"{__package__}.{filename}:app", port=8000, reload=True)
