import os
import configparser
from .database import connection

def Load():
	#Load configuration
	data_dir = os.environ.get('RECKONER_DATA_DIR', "./")
	config = configparser.ConfigParser()
	if os.path.isfile(data_dir + "reckoner.ini"):
		config.read_file(data_dir + "reckoner.ini")

	database: dict = {}
	if "DATABASE" in config:
		database = config["DATABASE"]

	#Database Setup
	connection.SetupConnection(database)