from fastapi import FastAPI
from . import admin, item, user, security

_prefix = "/api"

tags_metadata = [
	{
		"name": "admin",
		"description": "Admin operations to create users or modify other database values",
	},
	{
		"name": "users",
		"description": "Operations with users. The **login** logic is also here.",
	},
	{
		"name": "items",
		"description": "Manage items. So _fancy_ they have their own docs.",
		"externalDocs": {
			"description": "Items external docs",
			"url": "https://fastapi.tiangolo.com/",
		},
	},
	{
		"name": "security",
		"description": "Security endpoints to authenticate a user",
	},
]

def AddRoutes(app: FastAPI):
	app.include_router(admin.router, prefix=_prefix, tags=["admin"])
	app.include_router(item.router, prefix=_prefix, tags=["items"])
	app.include_router(user.router, prefix=_prefix, tags=["users"])
	app.include_router(security.router, prefix=_prefix, tags=["security"])