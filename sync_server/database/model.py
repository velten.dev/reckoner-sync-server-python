from copy import deepcopy
from datetime import datetime
from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, Integer, JSON, String, Text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects import postgresql, mysql, sqlite
from .connection import Base

# SQLAlchemy does not map BigInt to Int by default on the sqlite dialect.
# It should, but it doesnt.
BigIntegerType = BigInteger()
BigIntegerType = BigIntegerType.with_variant(postgresql.BIGINT(), 'postgresql')
BigIntegerType = BigIntegerType.with_variant(mysql.BIGINT(), 'mysql')
BigIntegerType = BigIntegerType.with_variant(sqlite.INTEGER(), 'sqlite')

class User(Base):
	__tablename__ = "users"
	id = Column(Integer, primary_key=True, index=True, autoincrement=True)
	name = Column(String(50), nullable=False, unique=True, index=True)
	email = Column(String(255), nullable=False, unique=True, index=True)
	password = Column(String(255), nullable=False)
	is_admin = Column(Boolean)
	public_key = Column(Text)
	private_key = Column(Text)
	totp_secret = Column(String(50))
	items = relationship("Item", cascade="all, delete")

	def __eq__(self, other) -> bool:
		if (not isinstance(self, other.__class__)): return False
		a, b = deepcopy(self.__dict__), deepcopy(other.__dict__)
		#compare based on equality our attributes, ignoring SQLAlchemy internal stuff
		a.pop('_sa_instance_state', None)
		b.pop('_sa_instance_state', None)
		return (a == b)
	def __ne__(self, other) -> bool:
		return not self.__eq__(other)

class Client(Base):
	__tablename__ = "clients"
	id = Column(Integer, primary_key=True, index=True, autoincrement=True)
	device_id = Column(String(255), nullable=False)
	secret = Column(String(255), nullable=False)


class Item(Base):
	__tablename__ = "items"
	id = Column(BigIntegerType, primary_key=True, index=True, autoincrement=True)
	user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
	update_inst = Column(DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)
	data = Column(JSON, nullable=False)
