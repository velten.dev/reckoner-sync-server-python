import time
from passlib.hash import argon2
from aioify import aioify

def _findDefaultDifficulty() -> int:
	rounds: int = 2
	computeTime = 0
	testPwd = "TestPassword"
	#Since each round equates to roughly double the time, 
	#find time which is over half the target time of 0.25 sec
	while computeTime <= 0.125:
		rounds += 1
		start = time.perf_counter()
		argon2.using(rounds=rounds).hash(testPwd)
		computeTime = time.perf_counter() - start
	return rounds

_rounds: int = _findDefaultDifficulty()
_asyncHash = aioify(argon2.using(rounds=_rounds).hash)
_asycnVerify = aioify(argon2.verify)

async def hash(password: str) -> str:
	return await _asyncHash(secret=password)

async def verify(candidate:str, password_hash: str) -> bool:
	return await _asycnVerify(secret=candidate, hash=password_hash)
